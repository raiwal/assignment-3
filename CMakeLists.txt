# set cmake minimum version
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(A3_Project VERSION 1.0)

enable_testing()

add_test(NAME Unit_Tests
         COMMAND runTests --gtest_output=xml:report.xml)

# include and link source folder
include_directories(src)

# link_directories(tests)
link_directories(src)
# add the executable
add_executable(main src/main.cpp)

# add add library
add_library(util_lib src/utils.cpp)

# link the library to the executable
target_link_libraries(main util_lib)

# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Link runTests with what we want to test and the GTest and pthread library
# add the executable to the runTests
add_executable(runTests src/util_test.cpp)
# add_executable(runTests src/utils.cpp)

# link the library to the executable
target_link_libraries(runTests util_lib)

target_link_libraries(runTests ${GTEST_LIBRARIES} pthread)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# make release the default build
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
  message(STATUS "Build type not specified: Use Release by default")
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O")

