# Assignment-3

Reads comma separeted input file. Validates file input and creates a summary of input that is written to the file 

## Table of Contents

 - [Validation / assumptions](#validation--assumptions)
 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Known errors](#known-errors)
 - [Authors](#authors)
 - [License](#license)

## Validation / assumptions

* Unix-file format newlines

* Items are separated with comma 

* Internal error code is -1

* Input file name is FleetState.txt and it is located in src, build or project root folder

* Program expects strict input format from **FleetState.txt**. Any devition will terminate program

## Requirements

Requires `gcc`, `cmake`, and `gtest`

## Usage

### Build release:

```
$ mkdir -p build && cd build
$ cmake ../
$ make
$ ./main # Run the compiled binary
```

### Build debug:

```
$ mkdir -p build && cd build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug # Generate makefile for debug build   
# Compile the project
$ make
$ ./main # Run the compiled binary
```

## Known errors
String to int conversion fails if input file id number is bigger what stoi can process.

## Authors 

[**Elmo Rohulla (@rootelmo)**](https://gitlab.com/rootElmo/)

[**Rainer Waltzer (@raiwal)**](https://gitlab.com/raiwal)

## License
CC0
