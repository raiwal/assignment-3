#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <array>
#include <stdio.h>
#include <unistd.h>
#include <filesystem>
#include <fstream>

#define GET_CUR_DIR getcwd
        
const int HOST_ID = 0;
const int INSTANCE_TYPE = 1;
const int TOTAL_SLOT_NUMBERS = 2;
const int ERROR = -1;
const std::string STRING_ERROR{""};
const int SLOT_START_POSITION = 3;

class Utils {

    public:

        struct report_data
        {
            std::string hostname;
            int empty_slots{0};
            int full_slots{0};
            int free_slots{0};
            int most_filed{0};
            int most_filed_hosts{0}; 
        };

        std::vector<report_data> report;
        std::vector<std::string> hostnames;
        std::array<std::string, 3> file_paths = {"/src/", "/build/", "/"};
        std::string file_name = "FleetState.txt";
        std::string path_to_fleet_data = "";
        std::vector<std::string> get_host_vector(std::string v_text);

        int get_host_id(std::vector<std::string> rawdata);
        std::string get_instance_type(std::vector<std::string> rawdata);
        int get_max_slots(std::vector<std::string> rawdata);
        bool verify_slots_count(int excpected_slots, std::vector<std::string> rawdata);
        bool verify_slots(std::vector<std::string> rawdata);
        bool verify_host_vectors(std::vector<std::vector<std::string>> hostvectors);

        // report processing functions
        bool slots_empty(std::vector<std::string> host);
        bool slots_full(std::vector<std::string> host);
        int get_available_slots(std::vector<std::string> host);
        int slots_used(std::vector<std::string> host);
        bool host_exist(std::string host);
        void add_host(std::string host); 

        void do_report(std::vector<std::vector<std::string>> hostvectors);
        void print_report();

        // file processing functions
        int check_file_exists();
        void set_file_path(std::string path_to_data);
        void write_output_file();
    private:
};
