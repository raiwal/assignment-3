#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "utils.h"

Utils util;
std::vector<std::vector<std::string>> hostvectors;

int main()
{   
    std::string line;
    std::fstream data_file;
      
    util.check_file_exists();

    data_file.open(util.path_to_fleet_data);
    if (data_file.is_open()) {
        while (getline(data_file, line)) {
            hostvectors.push_back(util.get_host_vector(line));
        }
        data_file.close();
    } else {
        std::cout << "Error!" << std::endl;
    }
    // verify host vectors. We accept only valid fileformat - which is not Windows format
    if (util.verify_host_vectors(hostvectors))
    {
        util.do_report(hostvectors);
        util.print_report();
        util.write_output_file();
    }
    else
    {
        std::cout << "Verification fails! Check input file format." << std::endl;
    }

    return 0;
}

