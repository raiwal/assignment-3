#include "utils.h"

std::vector<std::string> Utils::get_host_vector(std::string v_text) {
    
    std::vector<std::string> ret_vec;
    std::istringstream ss;
    ss.str(v_text);

    for (std::string s; std::getline(ss, s, ','); ) {    
        ret_vec.push_back(s);
    }
    return ret_vec;
}

int Utils::get_host_id(std::vector<std::string> rawdata)
{
    // Agree return error code for invalid values 
    int host_id{ERROR};

    if (rawdata.empty())
    {
        std::cerr << "Empty vector!" << std::endl;
        return host_id;
    }

    // stoi converts eg. 8R to 8 so we must checkt 
    // that every char is a digit
    for (auto c:rawdata.at(HOST_ID))
    {
        if(!isdigit(c))
        {
            return host_id;
        }
    }

    try
    {
        host_id = stoi(rawdata.at(HOST_ID));
    }
    catch(const std::out_of_range& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return host_id;
}

// Returns empty in case of error
std::string Utils::get_instance_type(std::vector<std::string> rawdata) 
{
    if (rawdata.size() < 2)
    {
        std::cout << "Corrupted vector!" << std::endl;
        return "";
    }
    else 
    {
          return rawdata.at(INSTANCE_TYPE);
    }
    return ""; 
};

// Return maximumm number of slots per instance from given strin
int Utils::get_max_slots(std::vector<std::string> rawdata)
{
    // Agree return error code for invalid values
    int max_slots{ERROR};
    // Size check so we knot at least something is at pos 3
    if (rawdata.size() < 3)
    {
        return max_slots;
    }
    // Every char is a digit
    for (auto c:rawdata.at(TOTAL_SLOT_NUMBERS))
    {
        if(!isdigit(c))
        {
            return max_slots;
        }
    }

    return stoi(rawdata.at(TOTAL_SLOT_NUMBERS));
}

int Utils::check_file_exists() {

    int ret_val = -1; // Return -1 by default

    // Get current path (should be <project root>/build)
    char current_path[FILENAME_MAX];

    if (!GET_CUR_DIR(current_path, sizeof(current_path))) {
        return errno;
    }

    current_path[sizeof(current_path) - 1] = '\0'; /* not really required */

    std::filesystem::path root_path = current_path;

    for (int i=0; i<(int) file_paths.size(); i++) {
        // Gets project folder root path (from "<project root>/build" to "<project root>/"")
        std::string path_to_file(root_path.parent_path());
        // Append one element of file_paths to the path we're checking
        path_to_file.append(file_paths[i]);
        // Append filename to check if file is at the location being checked
        path_to_file.append(file_name);

        // Check if file exists
        if (std::filesystem::exists(path_to_file)) {
            // std::cout << path_to_file << " exists!";
            set_file_path(path_to_file);
            ret_val = 1;

            // return as soon as file is found
            return ret_val;
        }
    }
    return ret_val;
}

void Utils::set_file_path(std::string path_to_data) {
    path_to_fleet_data = path_to_data;
}

// Total slot count is found from given string.
// That number must match to listed slots after that
bool Utils::verify_slots_count(int excpected_slots, std::vector<std::string> rawdata)
{
    return (excpected_slots == (int)(rawdata.size() - 3)); 
}

// Verify that each slot is either 0 or 1
bool Utils::verify_slots(std::vector<std::string> rawdata)
{
    if (rawdata.size() < 4)
    {
        return false; // can't have slots
    }
    
    for (int i = 3; i < (int)rawdata.size(); i++)
    {
        if(rawdata.at(i) != "0" && rawdata.at(i) != "1") // && rawdata.at(i) != "1\r" && rawdata.at(i) != "0\r")
        {
            return false;
        }
    }
    return true;
}

// Checks that a given hosts read data as a vector is a valid set of data
bool Utils::verify_host_vectors(std::vector<std::vector<std::string>> hostvectors)
{
    for(auto line:hostvectors)
    {
        if (get_host_id(line) == ERROR)
        {
            // std::cout << "Get host id fails" << std::endl;
            return false;
        }
        if (get_instance_type(line) == STRING_ERROR)
        {
            // std::cout << "Get instance type fails" << std::endl;
            return false;
        }
        if (get_max_slots(line) == ERROR)
        {
            // std::cout << "Get max slots fails" << std::endl;
            return false;
        }
        if (!verify_slots_count(get_max_slots(line), line))
        {
            // std::cout << "Verify slot count fails" << std::endl;
            return false;
        }
        if (!verify_slots(line))
        {
            // std::cout << "Get verify slots fails: " << std::endl;
            return false;
        }
     }
    return true;
}

// Check if this host slots are empty. Host must be in correct format when calling this.
bool Utils::slots_empty(std::vector<std::string> host)
{
    for (int i = SLOT_START_POSITION; i < (int)host.size(); i++)
    {
        if(host.at(i) != "0")
        {
            return false;
        }
    }
    return true;
}
// Check if this host slots are full -> true
bool Utils::slots_full(std::vector<std::string> host)
{
    for (int i = SLOT_START_POSITION; i < (int)host.size(); i++)
    {
        if(host.at(i) != "1")
        {
            return false;
        }
    }
    return true;
}
// return available free slots
int Utils::get_available_slots(std::vector<std::string> host)
{
    int empty_slots = 0;
    for (int i = SLOT_START_POSITION; i < (int)host.size(); i++)
    {
        if(host.at(i) != "1")
        {
            empty_slots++;
        }
    }

    return empty_slots;
}

// Return count of used slots of this host
int Utils::slots_used(std::vector<std::string> host)
{
    int used_slots = 0;
    for (int i = SLOT_START_POSITION; i < (int)host.size(); i++)
    {
        if(host.at(i) != "0")
        {
            used_slots++;
        }
    }

    return used_slots;    
}

// check if this is already known host
bool Utils::host_exist(std::string host)
{
    if(hostnames.empty())
    {
        return false; 
    }
    for (auto known_hosts : hostnames)
    {
        if (known_hosts == host)
        {
            return true;
        }
    }
    return false;
}

// Adds host to known hosts. Before calling this you
// must verify that host does not exist
void Utils::add_host(std::string host)
{
    hostnames.push_back(host);
}

void Utils::do_report(std::vector<std::vector<std::string>> hostvectors)
{   
    // First we create a list for known hosts
    for (auto host:hostvectors)
    {
        Utils::report_data report_template;
        std::string hostname = get_instance_type(host);
        if(!host_exist(hostname))
        {
            // Because this is first entry it is also a baseline for this instance stats
            add_host(hostname);
            report_template.hostname = hostname;
            report_template.free_slots = get_available_slots(host);
            if (slots_full(host))
            {
                report_template.full_slots++;
            }
            if (slots_empty(host))
            {
                report_template.empty_slots++;
            }
            report_template.most_filed = get_available_slots(host);
            if (report_template.most_filed != 0)
            {
                report_template.most_filed_hosts++;
            }
            
            report.push_back(report_template);
        }
        else // host exists so we need possibly update stats
        {
            // first we need to get access to existing data
            for (auto& existing_host_data : report)
            {
                // Ok, we found our existing data record
                if(hostname == existing_host_data.hostname)
                {
                    if(slots_empty(host))
                    {
                        existing_host_data.empty_slots++;
                    }
                    if(slots_full(host))
                    {
                        existing_host_data.full_slots++;
                    }
                    // First case: No full, smaller available value for existing which is not zero
                    if(!slots_full(host) && ((get_available_slots(host) < existing_host_data.most_filed) && (existing_host_data.most_filed != 0)))
                    {
                        // We desire here situation where we update smallest available slot count
                        existing_host_data.most_filed = get_available_slots(host);
                        existing_host_data.most_filed_hosts = 1; // This is new case so we need to do reset
                    }
                    // we might have found same count for available slots which is not initial state
                    else if (!slots_full(host) && ((get_available_slots(host) == existing_host_data.most_filed) && (existing_host_data.most_filed != 0)))
                    {
                        existing_host_data.most_filed_hosts++; // we just update count; 
                    }   
                    // We do not accept zero as available hosts
                    else if (!slots_empty(host) && (get_available_slots(host) > 0 && existing_host_data.most_filed == 0))
                    {
                        existing_host_data.most_filed = get_available_slots(host);
                        existing_host_data.most_filed_hosts = 1; // This is new case so we need to do reset
                    }
                    else
                    {
                        ; 
                    }
                }
            }
        }
    }
}

void Utils::print_report()
{
    std::cout << "EMPTY: ";
    for (auto r : report) {
        std::cout << r.hostname << "=" << r.empty_slots << ";";
    }
    std::cout << std::endl;
    // FULL
    std::cout << "FULL: ";
    for (auto r : report) {   
        std::cout << r.hostname << "=" << r.full_slots << "; ";
    }
    // MOST FILLED
    std::cout << "MOST FILLED: ";
    for (auto r : report) {
        std::cout << r.hostname << "=" << r.most_filed_hosts << "," << r.most_filed << ";";
    }
}

void Utils::write_output_file() {
    std::fstream out_file("Statistics.txt", out_file.out);
    if (!out_file.is_open()) {
        std::cout << "Can't open file!" << std::endl;
    } else {
        // EMPTY
        out_file << "EMPTY: ";
        for (auto r : report) {
            out_file << r.hostname << "=" << r.empty_slots << ";";
        }
        out_file << std::endl;
        // FULL
        out_file << "FULL: ";
        for (auto r : report) {   
            out_file << r.hostname << "=" << r.full_slots << "; ";
        }
        // MOST FILLED
        out_file << "MOST FILLED: ";
        for (auto r : report) {
            out_file << r.hostname << "=" << r.most_filed_hosts << "," << r.most_filed << ";";
        }
    }
    out_file.close();
}