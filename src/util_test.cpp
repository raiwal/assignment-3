#include "utils.h"
#include <gtest/gtest.h>

Utils test_util; // Util class to do tests with
std::vector<std::string> valid_test_line {"32", "M3", "5", "0", "1", "1", "0", "1"};
std::vector<std::string> host_empty_slots {"32", "M3", "5", "0", "0", "0", "0", "0"};
std::vector<std::string> host_full_slots {"32", "XX", "5", "1", "1", "1", "1", "1"};
std::vector<std::string> invalid_host_slot {"32", "M0", "4", "1", "1", "1", "1", "8"};
std::vector<std::string> invalid_host_slot_mark {"32", "M0"};
std::vector<std::string> invalid_host{"RW", "M3", "4", "1", "1", "1", "1", "8"};

std::string host{"New host"};

int test_file_locations(std::string f_name = "FleetState.txt") {
    test_util.file_name = f_name;
    return test_util.check_file_exists();
}

TEST(util_lib, Valid_cases)
{
    ASSERT_EQ(32, test_util.get_host_id(valid_test_line));
    ASSERT_EQ(ERROR, test_util.get_host_id(invalid_host));
    ASSERT_EQ("M3", test_util.get_instance_type(valid_test_line));
    ASSERT_EQ(5, test_util.get_max_slots(valid_test_line));
    ASSERT_EQ(true, test_util.verify_slots_count(5, valid_test_line));
    ASSERT_EQ(false, test_util.verify_slots_count(6, valid_test_line));
    ASSERT_EQ(true, test_util.verify_slots(valid_test_line));
    ASSERT_EQ(false, test_util.verify_slots(invalid_host_slot));
    ASSERT_EQ(false, test_util.host_exist(host));
    ASSERT_EQ(5, test_util.get_max_slots(valid_test_line));
    ASSERT_EQ(ERROR, test_util.get_max_slots(invalid_host_slot_mark));
}

TEST(util_lib, Invalid)
{
    ASSERT_NE(31, test_util.get_host_id(valid_test_line));
    ASSERT_NE("M2", test_util.get_instance_type(valid_test_line));
}

TEST(util_lib, Valid_report_cases)
{
    ASSERT_EQ(true, test_util.slots_empty(host_empty_slots));
    ASSERT_EQ(true, test_util.slots_full(host_full_slots));
    ASSERT_EQ(2, test_util.get_available_slots(valid_test_line));
    ASSERT_EQ(3, test_util.slots_used(valid_test_line));
    ASSERT_EQ(false, test_util.host_exist(host));
}

TEST(util_lib, Valid_files) {
    ASSERT_EQ(1, test_file_locations());
}

TEST(util_lib, Invalid_files) {
    ASSERT_EQ(-1, test_file_locations("fleetstate.txt"));
    ASSERT_EQ(-1, test_file_locations("fleetstate"));
    ASSERT_EQ(-1, test_file_locations("FleetState"));
    ASSERT_EQ(-1, test_file_locations("FLEETSTATE"));
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
